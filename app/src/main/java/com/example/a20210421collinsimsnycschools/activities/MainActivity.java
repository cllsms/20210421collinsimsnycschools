package com.example.a20210421collinsimsnycschools.activities;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.a20210421collinsimsnycschools.R;
import com.example.a20210421collinsimsnycschools.fragments.ItemDetailFragment;
import com.example.a20210421collinsimsnycschools.fragments.SchoolListFragment;
import com.example.a20210421collinsimsnycschools.network.core.NYCSchoolCore;
import com.example.a20210421collinsimsnycschools.network.core.NYCSchoolManager;
import com.example.a20210421collinsimsnycschools.network.models.SATResults;
import com.example.a20210421collinsimsnycschools.network.models.School;
import com.example.a20210421collinsimsnycschools.network.util.DisposableManager;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private NYCSchoolCore mNYCSchoolCore;
    private ArrayList<School> allNYCSchools;
    private DisposableManager mDisposableManager;
    /**
     * The fragment argument representing the School that this fragment
     * represents.
     */
    public static final String ARG_SCHOOL = "school";

    /**
     * The fragment argument representing the SAT Results that are associated
     * with the school in the details fragment.
     */
    public static final String ARG_SAT_RESULTS = "sat_results";
    /**
     * The fragment argument representing the School that this fragment
     * represents.
     */
    public static final String ARG_SCHOOLS_LIST = "schools_list";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);

        mNYCSchoolCore = NYCSchoolManager.getInstance();
        allNYCSchools = new ArrayList<>();
        mDisposableManager = new DisposableManager();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("NYC Schools");
        setSupportActionBar(toolbar);

        getSchools();
    }

    @Override
    protected void onDestroy() {
        mDisposableManager.dispose();
        mDisposableManager = null;

        super.onDestroy();
    }

    /**
     * Add a disposable that will be disposed during the applicable activity lifecycle event
     * @param disposable
     * @param <T>
     * @return
     */
    protected <T extends Disposable> T addDisposable(T disposable) {
        mDisposableManager.add(disposable);
        return disposable;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (allNYCSchools == null || allNYCSchools.isEmpty()) {
            getSchools();
        }
    }

    private void getSchools() {
        mNYCSchoolCore.getAllNYCSchools(new NYCSchoolCore.NYCSchoolsFetchCallback() {
            @Override
            public void onCompletionSuccess(List<School> response) {
                ArrayList<School> temp = new ArrayList<>(response.size());
                temp.addAll(response);
                allNYCSchools = temp;
                allNYCSchools.sort((o1, o2) -> o1.getSchoolName().compareToIgnoreCase(o2.getSchoolName()));
                Bundle arguments = new Bundle();
                arguments.putParcelableArrayList(ARG_SCHOOLS_LIST, allNYCSchools);
                SchoolListFragment fragment = new SchoolListFragment();
                fragment.setArguments(arguments);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayout, fragment, "school_list_layout")
                        .commit();
            }

            @Override
            public void onCompletionFailure(String failureMessage) {

            }
        });
    }

    public void getSATResultsForSchool(School school) {
        mNYCSchoolCore.getSATResultsForSchoolByDBN(school.getDBN(), new NYCSchoolCore.SATResultsFetchCallback() {
            @Override
            public void onSubscribe(Disposable d) {
                addDisposable(d);
            }

            @Override
            public void onCompletionSuccess(List<SATResults> response) {
                Bundle arguments = new Bundle();
                arguments.putParcelable(ARG_SCHOOL, school);
                if (response != null && !response.isEmpty()) arguments.putParcelable(ARG_SAT_RESULTS, response.get(0));
                ItemDetailFragment fragment = new ItemDetailFragment();
                fragment.setArguments(arguments);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frameLayout, fragment, "school_detail_layout")
                        .addToBackStack("school_detail_layout")
                        .commit();
            }

            @Override
            public void onCompletionFailure(String failureMessage) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            getSupportFragmentManager().popBackStack();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}