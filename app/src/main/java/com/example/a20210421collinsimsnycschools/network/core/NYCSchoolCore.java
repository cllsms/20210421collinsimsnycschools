package com.example.a20210421collinsimsnycschools.network.core;

import com.example.a20210421collinsimsnycschools.network.models.SATResults;
import com.example.a20210421collinsimsnycschools.network.models.School;

import java.util.List;

public interface NYCSchoolCore {

    /**
     * Retrieve list of all NYC Schools.
     */
    void getAllNYCSchools(NYCSchoolsFetchCallback callback);

    /**
     * Retrieve SAT Test Results for a specific school.
     */
    void getSATResultsForSchoolByDBN(String dbn, SATResultsFetchCallback callback);

    interface NYCSchoolsFetchCallback extends CoreBaseResponseListener<List<School>> {}
    interface SATResultsFetchCallback extends CoreBaseDisposableResponseListener<List<SATResults>> {}

}
