package com.example.a20210421collinsimsnycschools.network.util;

public class Endpoints {

    public static final String NYC_SCHOOLS = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json/";
    public static final String NYC_SAT_RESULTS = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json/";

}
