package com.example.a20210421collinsimsnycschools.network.core;

import android.util.Log;

import com.example.a20210421collinsimsnycschools.network.api.NYCSchoolsApiManager;
import com.example.a20210421collinsimsnycschools.network.models.SATResults;
import com.example.a20210421collinsimsnycschools.network.models.School;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class NYCSchoolManager implements NYCSchoolCore {

    private static volatile NYCSchoolManager sInstance;

    public static NYCSchoolManager getInstance() {
        if (sInstance == null) {
            synchronized (NYCSchoolManager.class) {
                if (sInstance == null) {
                    sInstance = new NYCSchoolManager();
                }
            }
        }
        return sInstance;
    }

    @Override
    public void getAllNYCSchools(NYCSchoolsFetchCallback callback) {
        NYCSchoolsApiManager.getAllNYCSchools(schools -> {

        }, new Observer<List<School>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<School> schools) {
                if (callback == null) return;

                if (schools == null || schools.isEmpty()) {
                    callback.onCompletionFailure("Network Call Failed");
                } else {
                    callback.onCompletionSuccess(schools);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                callback.onCompletionFailure("Network Call Failed");
            }

            @Override
            public void onComplete() {

            }
        });
    }

    @Override
    public void getSATResultsForSchoolByDBN(String dbn, SATResultsFetchCallback callback) {
        NYCSchoolsApiManager.getSATResultsForSchoolByDBN(dbn, new Consumer<List<SATResults>>() {
            @Override
            public void accept(List<SATResults> satResults) throws Exception {

            }
        }, new Observer<List<SATResults>>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull List<SATResults> satResults) {
                Log.d("Response", satResults.toString());
                if (callback == null) return;


                if (satResults == null) {
                    callback.onCompletionFailure("Network Call Failed");
                } else {
                    callback.onCompletionSuccess(satResults);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                callback.onCompletionFailure("Network Call Failed");
            }

            @Override
            public void onComplete() {

            }
        });
    }


}
