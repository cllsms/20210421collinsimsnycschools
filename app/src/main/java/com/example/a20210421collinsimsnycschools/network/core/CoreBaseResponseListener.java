package com.example.a20210421collinsimsnycschools.network.core;

/**
 * Base interface that handles callbacks for request responses
 *
 * @author <A HREF="mailto:luke.davis@wendys.com">Luke Davis</A>
 * @version 1.0.0
 * @since 3/10/15
 * <p/>
 * Copyright (c) 2014 Wendy's. All rights reserved.
 */
public interface CoreBaseResponseListener<T> {

    /**
     * Handles a success response from the server
     *
     * @param response {@link Object}
     */
    void onCompletionSuccess(T response);

    /**
     * Handles a failure response from the server
     *
     * @param failureMessage {@link String}
     */
    void onCompletionFailure(String failureMessage);
}
