package com.example.a20210421collinsimsnycschools.network.api;

import com.example.a20210421collinsimsnycschools.network.util.Endpoints;
import com.example.a20210421collinsimsnycschools.network.models.School;
import com.example.a20210421collinsimsnycschools.network.models.SATResults;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface NYCSchoolsApis {

    @GET(Endpoints.NYC_SCHOOLS)
    Observable<List<School>> getAllNYCSchools();

    @GET(Endpoints.NYC_SAT_RESULTS)
    Observable<List<SATResults>> getSATResultsForSchoolByDBN(@QueryMap Map<String, String> queryMap);

}
