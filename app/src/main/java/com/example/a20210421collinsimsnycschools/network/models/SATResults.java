package com.example.a20210421collinsimsnycschools.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SATResults implements Parcelable {

    @SerializedName("dbn")
    @Expose
    public String mDbn;
    @SerializedName("school_name")
    @Expose
    public String mSchoolName;
    @SerializedName("num_of_sat_test_takers")
    @Expose
    public String mNumOfSatTestTakers;
    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    public String mSatCriticalReadingAvgScore;
    @SerializedName("sat_math_avg_score")
    @Expose
    public String mSatMathAvgScore;
    @SerializedName("sat_writing_avg_score")
    @Expose
    public String mSatWritingAvgScore;

    public SATResults() {}

    protected SATResults(Parcel in) {
        mDbn = in.readString();
        mSchoolName = in.readString();
        mNumOfSatTestTakers = in.readString();
        mSatCriticalReadingAvgScore = in.readString();
        mSatMathAvgScore = in.readString();
        mSatWritingAvgScore = in.readString();
    }
    
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDbn);
        dest.writeString(mSchoolName);
        dest.writeString(mNumOfSatTestTakers);
        dest.writeString(mSatCriticalReadingAvgScore);
        dest.writeString(mSatMathAvgScore);
        dest.writeString(mSatWritingAvgScore);
    }
    
    public static final Creator<SATResults> CREATOR = new Creator<SATResults>() {
        @Override
        public SATResults createFromParcel(Parcel source) {
            return new SATResults(source);
        }

        @Override
        public SATResults[] newArray(int size) {
            return new SATResults[size];
        }
    };

    public String getDbn() {
        return mDbn;
    }

    public String getSchoolName() {
        return mSchoolName;
    }

    public String getNumOfSatTestTakers() {
        return mNumOfSatTestTakers;
    }

    public String getSatCriticalReadingAvgScore() {
        return mSatCriticalReadingAvgScore;
    }

    public String getSatMathAvgScore() {
        return mSatMathAvgScore;
    }

    public String getSatWritingAvgScore() {
        return mSatWritingAvgScore;
    }

    @NonNull
    @Override
    public String toString() {
        return mDbn + ", " + mSchoolName + ", " + mNumOfSatTestTakers + ", " + mSatCriticalReadingAvgScore  + ", " + mSatMathAvgScore + ", " + mSatWritingAvgScore;
    }
}
