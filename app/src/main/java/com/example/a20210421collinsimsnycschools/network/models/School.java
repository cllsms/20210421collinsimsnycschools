package com.example.a20210421collinsimsnycschools.network.models;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class School implements Parcelable {

    @SerializedName("dbn")
    @Expose
    private String mDBN;
    @SerializedName("school_name")
    @Expose
    private String mSchoolName;
    @SerializedName("boro")
    @Expose
    private String mBoro;
    @SerializedName("overview_paragraph")
    @Expose
    private String mOverviewParagraph;
    @SerializedName("school_10th_seats")
    @Expose
    private String mSchool10thSeats;
    @SerializedName("academicopportunities1")
    @Expose
    private String mAcademicOpportunities1;
    @SerializedName("academicopportunities2")
    @Expose
    private String mAcademicOpportunities2;
    @SerializedName("ell_programs")
    @Expose
    private String mEllPrograms;
    @SerializedName("neighborhood")
    @Expose
    private String mNeighborhood;
    @SerializedName("building_code")
    @Expose
    private String mBuildingCode;
    @SerializedName("location")
    @Expose
    private String mLocation;
    @SerializedName("phone_number")
    @Expose
    private String mPhoneNumber;
    @SerializedName("fax_number")
    @Expose
    private String mFaxNumber;
    @SerializedName("school_email")
    @Expose
    private String mSchoolEmail;
    @SerializedName("website")
    @Expose
    private String mWebsite;
    @SerializedName("subway")
    @Expose
    private String mSubway;
    @SerializedName("bus")
    @Expose
    private String mBus;
    @SerializedName("grades2018")
    @Expose
    private String mGrades2018;
    @SerializedName("finalgrades")
    @Expose
    private String mFinalGrades;
    @SerializedName("total_students")
    @Expose
    private String mTotalStudents;
    @SerializedName("extracurricular_activities")
    @Expose
    private String mExtracurricularActivities;
    @SerializedName("school_sports")
    @Expose
    private String mSchoolSports;
    @SerializedName("attendance_rate")
    @Expose
    private String mAttendanceRate;
    @SerializedName("pct_stu_enough_variety")
    @Expose
    private String mPctStuEnoughVariety;
    @SerializedName("pct_stu_safe")
    @Expose
    private String mPctStuSafe;
    @SerializedName("school_accessibility_description")
    @Expose
    private String mSchoolAccessibilityDescription;
    @SerializedName("directions1")
    @Expose
    private String mDirections1;
    @SerializedName("requirement1_1")
    @Expose
    private String mRequirement1_1;
    @SerializedName("requirement2_1")
    @Expose
    private String mRequirement2_1;
    @SerializedName("requirement3_1")
    @Expose
    private String mRequirement3_1;
    @SerializedName("requirement4_1")
    @Expose
    private String mRequirement4_1;
    @SerializedName("requirement5_1")
    @Expose
    private String mRequirement5_1;
    @SerializedName("offer_rate1")
    @Expose
    private String mOfferRate1;
    @SerializedName("program1")
    @Expose
    private String mProgram1;
    @SerializedName("code1")
    @Expose
    private String mCode1;
    @SerializedName("interest1")
    @Expose
    private String mInterest1;
    @SerializedName("method1")
    @Expose
    private String mMethod1;
    @SerializedName("seats9ge1")
    @Expose
    private String mSeats9ge1;
    @SerializedName("grade9gefilledflag1")
    @Expose
    private String mGrade9gefilledflag1;
    @SerializedName("grade9geapplicants1")
    @Expose
    private String mGrade9geapplicants1;
    @SerializedName("seats9swd1")
    @Expose
    private String mSeats9swd1;
    @SerializedName("grade9swdfilledflag1")
    @Expose
    private String mGrade9swdfilledflag1;
    @SerializedName("grade9swdapplicants1")
    @Expose
    private String mGrade9swdapplicants1;
    @SerializedName("seats101")
    @Expose
    private String mSeats101;
    @SerializedName("admissionspriority11")
    @Expose
    private String mAdmissionsPriority11;
    @SerializedName("admissionspriority21")
    @Expose
    private String mAdmissionsPriority21;
    @SerializedName("admissionspriority31")
    @Expose
    private String mAdmissionsPriority31;
    @SerializedName("grade9geapplicantsperseat1")
    @Expose
    private String mGrade9geapplicantsperseat1;
    @SerializedName("grade9swdapplicantsperseat1")
    @Expose
    private String mGrade9swdapplicantsperseat1;
    @SerializedName("primary_address_line_1")
    @Expose
    private String mPrimaryAddressLine1;
    @SerializedName("city")
    @Expose
    private String mCity;
    @SerializedName("zip")
    @Expose
    private String mZip;
    @SerializedName("state_code")
    @Expose
    private String mStateCode;
    @SerializedName("latitude")
    @Expose
    private String mLatitude;
    @SerializedName("longitude")
    @Expose
    private String mLongitude;
    @SerializedName("community_board")
    @Expose
    private String mCommunityBoard;
    @SerializedName("council_district")
    @Expose
    private String mCouncilDistrict;
    @SerializedName("census_tract")
    @Expose
    private String mCensusTract;
    @SerializedName("bin")
    @Expose
    private String mBin;
    @SerializedName("bbl")
    @Expose
    private String mBbl;
    @SerializedName("nta")
    @Expose
    private String mNta;
    @SerializedName("borough")
    @Expose
    private String mBorough;

    protected School(Parcel in) {
        mDBN = in.readString();
        mSchoolName = in.readString();
        mBoro = in.readString();
        mOverviewParagraph = in.readString();
        mSchool10thSeats = in.readString();
        mAcademicOpportunities1 = in.readString();
        mAcademicOpportunities2 = in.readString();
        mEllPrograms = in.readString();
        mNeighborhood = in.readString();
        mBuildingCode = in.readString();
        mLocation = in.readString();
        mPhoneNumber = in.readString();
        mFaxNumber = in.readString();
        mSchoolEmail = in.readString();
        mWebsite = in.readString();
        mSubway = in.readString();
        mBus = in.readString();
        mGrades2018 = in.readString();
        mFinalGrades = in.readString();
        mTotalStudents = in.readString();
        mExtracurricularActivities = in.readString();
        mSchoolSports = in.readString();
        mAttendanceRate = in.readString();
        mPctStuEnoughVariety = in.readString();
        mPctStuSafe = in.readString();
        mSchoolAccessibilityDescription = in.readString();
        mDirections1 = in.readString();
        mRequirement1_1 = in.readString();
        mRequirement2_1 = in.readString();
        mRequirement3_1 = in.readString();
        mRequirement4_1 = in.readString();
        mRequirement5_1 = in.readString();
        mOfferRate1 = in.readString();
        mProgram1 = in.readString();
        mCode1 = in.readString();
        mInterest1 = in.readString();
        mMethod1 = in.readString();
        mSeats9ge1 = in.readString();
        mGrade9gefilledflag1 = in.readString();
        mGrade9geapplicants1 = in.readString();
        mSeats9swd1 = in.readString();
        mGrade9swdfilledflag1 = in.readString();
        mGrade9swdapplicants1 = in.readString();
        mSeats101 = in.readString();
        mAdmissionsPriority11 = in.readString();
        mAdmissionsPriority21 = in.readString();
        mAdmissionsPriority31 = in.readString();
        mGrade9geapplicantsperseat1 = in.readString();
        mGrade9swdapplicantsperseat1 = in.readString();
        mPrimaryAddressLine1 = in.readString();
        mCity = in.readString();
        mZip = in.readString();
        mStateCode = in.readString();
        mLatitude = in.readString();
        mLongitude = in.readString();
        mCommunityBoard = in.readString();
        mCouncilDistrict = in.readString();
        mCensusTract = in.readString();
        mBin = in.readString();
        mBbl = in.readString();
        mNta = in.readString();
        mBorough = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDBN);
        dest.writeString(mSchoolName);
        dest.writeString(mBoro);
        dest.writeString(mOverviewParagraph);
        dest.writeString(mSchool10thSeats);
        dest.writeString(mAcademicOpportunities1);
        dest.writeString(mAcademicOpportunities2);
        dest.writeString(mEllPrograms);
        dest.writeString(mNeighborhood);
        dest.writeString(mBuildingCode);
        dest.writeString(mLocation);
        dest.writeString(mPhoneNumber);
        dest.writeString(mFaxNumber);
        dest.writeString(mSchoolEmail);
        dest.writeString(mWebsite);
        dest.writeString(mSubway);
        dest.writeString(mBus);
        dest.writeString(mGrades2018);
        dest.writeString(mFinalGrades);
        dest.writeString(mTotalStudents);
        dest.writeString(mExtracurricularActivities);
        dest.writeString(mSchoolSports);
        dest.writeString(mAttendanceRate);
        dest.writeString(mPctStuEnoughVariety);
        dest.writeString(mPctStuSafe);
        dest.writeString(mSchoolAccessibilityDescription);
        dest.writeString(mDirections1);
        dest.writeString(mRequirement1_1);
        dest.writeString(mRequirement2_1);
        dest.writeString(mRequirement3_1);
        dest.writeString(mRequirement4_1);
        dest.writeString(mRequirement5_1);
        dest.writeString(mOfferRate1);
        dest.writeString(mProgram1);
        dest.writeString(mCode1);
        dest.writeString(mInterest1);
        dest.writeString(mMethod1);
        dest.writeString(mSeats9ge1);
        dest.writeString(mGrade9gefilledflag1);
        dest.writeString(mGrade9geapplicants1);
        dest.writeString(mSeats9swd1);
        dest.writeString(mGrade9swdfilledflag1);
        dest.writeString(mGrade9swdapplicants1);
        dest.writeString(mSeats101);
        dest.writeString(mAdmissionsPriority11);
        dest.writeString(mAdmissionsPriority21);
        dest.writeString(mAdmissionsPriority31);
        dest.writeString(mGrade9geapplicantsperseat1);
        dest.writeString(mGrade9swdapplicantsperseat1);
        dest.writeString(mPrimaryAddressLine1);
        dest.writeString(mCity);
        dest.writeString(mZip);
        dest.writeString(mStateCode);
        dest.writeString(mLatitude);
        dest.writeString(mLongitude);
        dest.writeString(mCommunityBoard);
        dest.writeString(mCouncilDistrict);
        dest.writeString(mCensusTract);
        dest.writeString(mBin);
        dest.writeString(mBbl);
        dest.writeString(mNta);
        dest.writeString(mBorough);
    }
    
    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel source) {
            return new School(source);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    public String getDBN() {
        return mDBN;
    }

    public String getSchoolName() {
        return mSchoolName;
    }

    public String getBoro() {
        return mBoro;
    }

    public String getOverviewParagraph() {
        return mOverviewParagraph;
    }

    public String getSchool10thSeats() {
        return mSchool10thSeats;
    }

    public String getAcademicOpportunities1() {
        return mAcademicOpportunities1;
    }

    public String getAcademicOpportunities2() {
        return mAcademicOpportunities2;
    }

    public String getEllPrograms() {
        return mEllPrograms;
    }

    public String getNeighborhood() {
        return mNeighborhood;
    }

    public String getBuildingCode() {
        return mBuildingCode;
    }

    public String getLocation() {
        return mLocation;
    }

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public String getFaxNumber() {
        return mFaxNumber;
    }

    public String getSchoolEmail() {
        return mSchoolEmail;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public String getSubway() {
        return mSubway;
    }

    public String getBus() {
        return mBus;
    }

    public String getGrades2018() {
        return mGrades2018;
    }

    public String getFinalGrades() {
        return mFinalGrades;
    }

    public String getTotalStudents() {
        return mTotalStudents;
    }

    public String getExtracurricularActivities() {
        return mExtracurricularActivities;
    }

    public String getSchoolSports() {
        return mSchoolSports;
    }

    public String getAttendanceRate() {
        return mAttendanceRate;
    }

    public String getPctStuEnoughVariety() {
        return mPctStuEnoughVariety;
    }

    public String getPctStuSafe() {
        return mPctStuSafe;
    }

    public String getSchoolAccessibilityDescription() {
        return mSchoolAccessibilityDescription;
    }

    public String getDirections1() {
        return mDirections1;
    }

    public String getRequirement1_1() {
        return mRequirement1_1;
    }

    public String getRequirement2_1() {
        return mRequirement2_1;
    }

    public String getRequirement3_1() {
        return mRequirement3_1;
    }

    public String getRequirement4_1() {
        return mRequirement4_1;
    }

    public String getRequirement5_1() {
        return mRequirement5_1;
    }

    public String getOfferRate1() {
        return mOfferRate1;
    }

    public String getProgram1() {
        return mProgram1;
    }

    public String getCode1() {
        return mCode1;
    }

    public String getInterest1() {
        return mInterest1;
    }

    public String getMethod1() {
        return mMethod1;
    }

    public String getSeats9ge1() {
        return mSeats9ge1;
    }

    public String getGrade9gefilledflag1() {
        return mGrade9gefilledflag1;
    }

    public String getGrade9geapplicants1() {
        return mGrade9geapplicants1;
    }

    public String getSeats9swd1() {
        return mSeats9swd1;
    }

    public String getGrade9swdfilledflag1() {
        return mGrade9swdfilledflag1;
    }

    public String getGrade9swdapplicants1() {
        return mGrade9swdapplicants1;
    }

    public String getSeats101() {
        return mSeats101;
    }

    public String getAdmissionsPriority11() {
        return mAdmissionsPriority11;
    }

    public String getAdmissionsPriority21() {
        return mAdmissionsPriority21;
    }

    public String getAdmissionsPriority31() {
        return mAdmissionsPriority31;
    }

    public String getGrade9geapplicantsperseat1() {
        return mGrade9geapplicantsperseat1;
    }

    public String getGrade9swdapplicantsperseat1() {
        return mGrade9swdapplicantsperseat1;
    }

    public String getPrimaryAddressLine1() {
        return mPrimaryAddressLine1;
    }

    public String getCity() {
        return mCity;
    }

    public String getZip() {
        return mZip;
    }

    public String getStateCode() {
        return mStateCode;
    }

    public String getLatitude() {
        return mLatitude;
    }

    public String getLongitude() {
        return mLongitude;
    }

    public String getCommunityBoard() {
        return mCommunityBoard;
    }

    public String getCouncilDistrict() {
        return mCouncilDistrict;
    }

    public String getCensusTract() {
        return mCensusTract;
    }

    public String getBin() {
        return mBin;
    }

    public String getBbl() {
        return mBbl;
    }

    public String getNta() {
        return mNta;
    }

    public String getBorough() {
        return mBorough;
    }


}
