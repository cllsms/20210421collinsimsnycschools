package com.example.a20210421collinsimsnycschools.network.core;

import io.reactivex.disposables.Disposable;

public interface CoreBaseDisposableResponseListener<T> extends CoreBaseResponseListener<T> {

    /**
     * Returns the disposable when the call begins.
     *
     * @param d {@link Disposable}
     */
    void onSubscribe(Disposable d);
}
