package com.example.a20210421collinsimsnycschools.network.util;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * A Wrapper for managing Disposables
 */
public class DisposableManager {

    private final CompositeDisposable mCompositeDisposable;

    /**
     * Default Constructor
     */
    public DisposableManager() {
        mCompositeDisposable = new CompositeDisposable();
    }

    private CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    /**
     * {@link CompositeDisposable#add(Disposable)}
     */
    public void add(Disposable disposable) {
        getCompositeDisposable().add(disposable);
    }

    /**
     * {@link CompositeDisposable#remove(Disposable)}
     */
    public void remove(Disposable disposable) {
        getCompositeDisposable().remove(disposable);
    }

    /**
     * {@link Disposable#dispose()}
     */
    public void dispose() {
        getCompositeDisposable().dispose();
    }

    /**
     * {@link Disposable#isDisposed()}
     */
    public boolean isDisposed() {
        return getCompositeDisposable().isDisposed();
    }
}
