package com.example.a20210421collinsimsnycschools.network.api;

import android.app.Application;

import com.example.a20210421collinsimsnycschools.NYCSchoolsApplication;
import com.example.a20210421collinsimsnycschools.network.util.NetworkUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseApiManager {
    private static final int CONNECT_TIMEOUT = 10;
    private static final int WRITE_TIMEOUT = 20;
    private static final int READ_TIMEOUT = 30;

    static Cache sCacheHolder;

    private static final String CACHE_HOLDER_FOLDER_NAME = "httpCache";
    private static final int CACHE_HOLDER_MAX_SIZE = 10 * 1024 * 1024; // 10 MB

    static final int CACHE_MAX_AGE = 60; // 1 min: rewrite cache if last cache was more than
    static final int CACHE_MAX_STALE = 60 * 60 * 24 * 28; // tolerate 4-weeks stale

    public static Retrofit getRetrofitForUrl(HttpUrl endpoint) {
        Gson gson = new GsonBuilder().setLenient().create();

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(endpoint);

        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
//        if (sCacheHolder == null) {
//            File httpCacheDirectory = new File(NYCSchoolsApplication.getInstance().getCacheDir(), CACHE_HOLDER_FOLDER_NAME);
//            sCacheHolder = new Cache(httpCacheDirectory, CACHE_HOLDER_MAX_SIZE);
//        }
//        okHttpClientBuilder.cache(sCacheHolder);

        okHttpClientBuilder.addInterceptor(chain -> {
            Request origReq = chain.request();

            Request.Builder requestBuilder = origReq.newBuilder()
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json");

            Request request = requestBuilder.build();
            return chain.proceed(request);
        });

//        okHttpClientBuilder.addNetworkInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Interceptor.Chain chain) throws IOException {
//                Response response = chain.proceed(chain.request());
//                if (NetworkUtil.isConnected()) {
//                    return response.newBuilder()
//                            .header("Cache-Control", "public, max-age=" + CACHE_MAX_AGE)
//                            .build();
//                } else {
//                    return response.newBuilder()
//                            .header("Cache-Control", "public, only-if-cached, max-stale=" + CACHE_MAX_STALE)
//                            .build();
//                }
//
//            }
//        });

        OkHttpClient okHttpClient = okHttpClientBuilder
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .build();
        retrofitBuilder.client(okHttpClient);

        return retrofitBuilder.build();
    }
}
