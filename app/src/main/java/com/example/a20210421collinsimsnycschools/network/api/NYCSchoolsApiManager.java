package com.example.a20210421collinsimsnycschools.network.api;

import com.example.a20210421collinsimsnycschools.network.models.SATResults;
import com.example.a20210421collinsimsnycschools.network.models.School;
import com.example.a20210421collinsimsnycschools.network.util.Endpoints;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;

public class NYCSchoolsApiManager extends BaseApiManager {
    private static Map<String, String> queryMap;

    public static void getAllNYCSchools(Consumer<List<School>> schoolConsumer, Observer<List<School>> schoolObserver) {
        NYCSchoolsApis nycSchoolsApis = getRetrofitForUrl(HttpUrl.parse(Endpoints.NYC_SCHOOLS)).create(NYCSchoolsApis.class);

        Observable<List<School>> schoolObservable = nycSchoolsApis.getAllNYCSchools();
        schoolObservable.subscribeOn(Schedulers.io()).doOnNext(schoolConsumer).observeOn(AndroidSchedulers.mainThread()).subscribe(schoolObserver);
    }

    public static void getSATResultsForSchoolByDBN(String dbn, Consumer<List<SATResults>> satResultsConsumer, Observer<List<SATResults>> satResultsObserver) {
        NYCSchoolsApis nycSchoolsApis = getRetrofitForUrl(HttpUrl.parse(Endpoints.NYC_SAT_RESULTS)).create(NYCSchoolsApis.class);

        queryMap = new HashMap<>();
        queryMap.put("dbn", dbn);

        Observable<List<SATResults>> satResultsObservable = nycSchoolsApis.getSATResultsForSchoolByDBN(queryMap);
        satResultsObservable.subscribeOn(Schedulers.io()).doOnNext(satResultsConsumer).observeOn(AndroidSchedulers.mainThread()).subscribe(satResultsObserver);
    }
}
