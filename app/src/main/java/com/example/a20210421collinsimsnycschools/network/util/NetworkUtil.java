package com.example.a20210421collinsimsnycschools.network.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.example.a20210421collinsimsnycschools.NYCSchoolsApplication;

public class NetworkUtil {

    public static boolean isConnected() {
        try {
            ConnectivityManager e = (ConnectivityManager) NYCSchoolsApplication.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = e.getActiveNetworkInfo();
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        } catch (Exception e) {
            Log.w("NetworkUtil", e.toString());
        }

        return false;
    }
}
