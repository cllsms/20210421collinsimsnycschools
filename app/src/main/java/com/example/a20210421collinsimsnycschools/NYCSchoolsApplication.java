package com.example.a20210421collinsimsnycschools;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.io.File;

public class NYCSchoolsApplication extends Application {
    public static NYCSchoolsApplication sInstance;

    @Override
    public void onCreate() {
        super.onCreate();

        sInstance = this;
    }

    public static NYCSchoolsApplication getInstance() {
        return sInstance;
    }

    private void clearApplicationData() {
        Log.w("CLEANING", "cleaning all app data");

        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists() && appDir.isDirectory()) {
            String[] children = appDir.list();
            for (String s : children) {
                // don't delete 'lib' or 'shared_prefs' folders
                if (!s.equals("lib") && !s.equals("shared_prefs")) {
                    deleteDir(new File(appDir, s));
                    Log.i("TAG", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();

            if (children != null) {
                for (String strChildren : children) {
                    boolean success = deleteDir(new File(dir, strChildren));
                    if (!success) {
                        return false;
                    }
                }
            }
        }

        return dir != null && dir.delete();
    }
}
