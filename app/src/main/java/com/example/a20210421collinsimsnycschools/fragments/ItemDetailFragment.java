package com.example.a20210421collinsimsnycschools.fragments;

import android.os.Bundle;

import com.example.a20210421collinsimsnycschools.activities.MainActivity;
import com.example.a20210421collinsimsnycschools.R;
import com.example.a20210421collinsimsnycschools.network.models.SATResults;
import com.example.a20210421collinsimsnycschools.network.models.School;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TextView;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link MainActivity}
 * in two-pane mode (on tablets) or a {@link ItemDetailActivity}
 * on handsets.
 */
public class ItemDetailFragment extends Fragment {
    /**
     * The School this fragment is presenting.
     */
    private School mSchool;

    /**
     * The School this fragment is presenting.
     */
    private SATResults mSATResults;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ItemDetailFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(MainActivity.ARG_SCHOOL)) {
            mSchool = (School) getArguments().get(MainActivity.ARG_SCHOOL);
        }

        if (getArguments().containsKey(MainActivity.ARG_SAT_RESULTS)) {
            mSATResults = (SATResults) getArguments().get(MainActivity.ARG_SAT_RESULTS);
        }

        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_detail, container, false);

        // Show the School information as text in a TextView.
        if (mSchool != null) {
            ((TextView) rootView.findViewById(R.id.school_name)).setText(mSchool.getSchoolName());
            ((TextView) rootView.findViewById(R.id.item_detail)).setText(mSchool.getOverviewParagraph());
            ((TextView) rootView.findViewById(R.id.school_primary_address)).setText(mSchool.getPrimaryAddressLine1());
            ((TextView) rootView.findViewById(R.id.school_city)).setText(mSchool.getCity());
            ((TextView) rootView.findViewById(R.id.school_state)).setText(mSchool.getStateCode());
            ((TextView) rootView.findViewById(R.id.school_zip)).setText(mSchool.getZip());
            ((TextView) rootView.findViewById(R.id.school_website)).setText(mSchool.getWebsite());
        }

        // Show the School's SAT information as text in a TextView.
        if (mSATResults != null) {
            ((TextView) rootView.findViewById(R.id.sat_results_title)).setVisibility(View.VISIBLE);
            ((TableLayout) rootView.findViewById(R.id.sat_results_table_layout)).setVisibility(View.VISIBLE);
            ((TextView) rootView.findViewById(R.id.sat_results_num_of_takers)).setText(mSATResults.getNumOfSatTestTakers());
            ((TextView) rootView.findViewById(R.id.sat_results_reading_avg_score)).setText(mSATResults.getSatCriticalReadingAvgScore());
            ((TextView) rootView.findViewById(R.id.sat_results_math_avg_score)).setText(mSATResults.getSatMathAvgScore());
            ((TextView) rootView.findViewById(R.id.sat_results_writing_avg_score)).setText(mSATResults.getSatWritingAvgScore());
        }

        return rootView;
    }


}