package com.example.a20210421collinsimsnycschools.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20210421collinsimsnycschools.R;
import com.example.a20210421collinsimsnycschools.activities.MainActivity;
import com.example.a20210421collinsimsnycschools.adapters.SchoolRecyclerViewAdapter;
import com.example.a20210421collinsimsnycschools.adapters.SimpleDividerItemDecoration;
import com.example.a20210421collinsimsnycschools.network.models.School;

import java.util.ArrayList;

public class SchoolListFragment extends Fragment {
    private ArrayList<School> allNYCSchools;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        allNYCSchools = new ArrayList<>();

        if (getArguments().containsKey(MainActivity.ARG_SCHOOLS_LIST)) {
            allNYCSchools = getArguments().getParcelableArrayList(MainActivity.ARG_SCHOOLS_LIST);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_list, container, false);
        setupRecyclerView(((RecyclerView)rootView.findViewById(R.id.item_list)));
        return rootView;
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity()));
        recyclerView.setAdapter(new SchoolRecyclerViewAdapter(((MainActivity)getActivity()), allNYCSchools));
    }
}
