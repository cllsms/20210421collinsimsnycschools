package com.example.a20210421collinsimsnycschools.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20210421collinsimsnycschools.R;
import com.example.a20210421collinsimsnycschools.activities.MainActivity;
import com.example.a20210421collinsimsnycschools.network.models.School;

import java.util.List;

public class SchoolRecyclerViewAdapter extends RecyclerView.Adapter<SchoolRecyclerViewAdapter.ViewHolder> {

    private final MainActivity mParentActivity;
    private final List<School> mValues;
    private final View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            School item = (School) view.getTag();
            mParentActivity.getSATResultsForSchool(item);
        }
    };

    public SchoolRecyclerViewAdapter(MainActivity parent,
                                     List<School> items) {
        mValues = items;
        mParentActivity = parent;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_content, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        School temp = mValues.get(position);

        holder.mIdView.setText(temp.getDBN());
        holder.mContentView.setText(temp.getSchoolName());

        holder.itemView.setTag(temp);
        holder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final TextView mIdView;
        final TextView mContentView;

        ViewHolder(View view) {
            super(view);
            mIdView = (TextView) view.findViewById(R.id.id_text);
            mContentView = (TextView) view.findViewById(R.id.content);
        }
    }
}
